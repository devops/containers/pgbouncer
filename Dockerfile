FROM gitlab-registry.oit.duke.edu/devops/containers/debian-buster:latest

ARG pgbouncer_download_url
ARG pgbouncer_download_sha256

RUN set -eux; \
	apt-get -y update; \
	apt-get -y install \
	gcc \
	libc-ares2 \
	libc-ares-dev \
	libevent-2.1-6 \
	libevent-dev \
	libpam0g \
	libssl1.1 \
	libssl-dev \
	lsb-base \
	make \
	pkg-config \
	python3 \
	python3-psycopg2 \
	; \
	apt-get -y clean; \
	rm -rf /var/lib/apt/lists/*

# Download
RUN set -eux; \
	curl -s -o pgbouncer.tar.gz $pgbouncer_download_url; \
	echo "$pgbouncer_download_sha256  pgbouncer.tar.gz" | sha256sum -c --strict; \
	mkdir -p /usr/src/pgbouncer; \
        tar -zxf pgbouncer.tar.gz -C /usr/src/pgbouncer --strip-components=1; \
	rm pgbouncer.tar.gz

# Install
RUN set -eux; \
	cd /usr/src/pgbouncer; \
	./configure --prefix=/usr/local; \
	make; \
	make install; \
	cd /usr/local; \
	rm -r /usr/src/pgbouncer

COPY ./etc/ /etc/

RUN set -eux; \
	groupadd -r pgbouncer; \
	useradd -r -g pgbouncer pgbouncer; \
	mkdir -p /etc/pgbouncer /var/log/pgbouncer /var/run/pgbouncer; \
	# cp /usr/local/share/doc/pgbouncer/pgbouncer.ini /etc/pgbouncer/; \
	# touch /etc/pgbouncer/userlist.txt; \
	chown -R pgbouncer:pgbouncer /etc/pgbouncer /var/log/pgbouncer /var/run/pgbouncer

USER pgbouncer

CMD [ "pgbouncer", "/etc/pgbouncer/pgbouncer.ini" ]
