SHELL = /bin/bash

build_tag ?= pgbouncer
pgbouncer_download_url = http://www.pgbouncer.org/downloads/files/1.14.0/pgbouncer-1.14.0.tar.gz
pgbouncer_download_sha256 = a0c13d10148f557e36ff7ed31793abb7a49e1f8b09aa2d4695d1c28fa101fee7

.PHONY : build
build:
	docker build --pull -t $(build_tag) \
		--build-arg pgbouncer_download_url=$(pgbouncer_download_url) \
		--build-arg pgbouncer_download_sha256=$(pgbouncer_download_sha256) \
		.

.PHONY : test
test:
	docker-compose -f test/docker-compose.yml up --exit-code-from client
